package day4;

import java.util.Arrays;
import java.util.Scanner;

public class Soal2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Masukan Sinyal = ");
		String sinyal = input.nextLine().toUpperCase();
		
		int salah =0;
		
		for (int i = 0; i < sinyal.length(); i+=3) {
			String sub1 = sinyal.substring(i, i + 3);
			if (!sub1.equals("SOS")) {
				salah += 1;  
			}
		} System.out.println(salah);
		
	}
}
