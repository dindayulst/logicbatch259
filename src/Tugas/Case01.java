package Tugas;

import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;
import java.util.*;

public class Case01 {
	public static void Resolve1() {
		Scanner input = new Scanner(System.in);
		System.out.print("Masukan waktu parkir = ");
		String jamParkir = input.next();

		System.out.print("Masukan waktu keluar = ");
		String jamKeluar = input.next();

		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm");

		java.util.Date jamAwal = null;
		java.util.Date jamAkhir = null;

		try {
			jamAwal = format.parse(jamParkir);
			jamAkhir = format.parse(jamKeluar);
			double diff = jamAkhir.getTime() - jamAwal.getTime();
			double diffJam = diff / (60 * 60 * 1000);

		} catch (ParseException e) {
			e.printStackTrace();
		}

//		String jam2 = jamKeluar.substring(0 , 2);
//		String menit2 = jamKeluar.substring(3); 
//		
//		//mengubah string menjadi integer
//		int jamInteger = Integer.parseInt(jam);
//		int menitInteger = Integer.parseInt(menit);
//		int jamInteger2 = Integer.parseInt(jam2);
//		int menitInteger2 = Integer.parseInt(menit2);
//	
//		int waktu1 = (3600*jamInteger) + (60*menitInteger);
//		int waktu2 = (3600*jamInteger2) + (60*menitInteger2);
//		int selisih = waktu2 - waktu1;
//		int j3 = selisih/3600;
//		int sisa = selisih % 3600;
//		int m3 = sisa / 60;
//		
//		System.out.println("Lama Parkir = " + j3 + " Jam " + m3 + " Menit");
//		
//		int jamBulat = 0;
//		if (m3 > 30) {
//			jamBulat = j3+1;
//			System.out.println("Pembulatan = " +jamBulat+ " Jam");
//			System.out.println("Tarif = " + (jamBulat * 3000));
//		}else {
//			System.out.println("Pembulatan = " + j3 + " Jam");
//			System.out.println("Tarif = Rp" + (j3 * 3000));
//		}
	}

	public static void Resolve2() {
		Scanner input = new Scanner(System.in);
		System.out.print("Tanggal Pinjam = ");
		String stglAwal = input.nextLine();

		System.out.println("Lama Pinjam = ");
		int lamaPinjam = input.nextInt();
		input.nextLine();

		System.out.print("Tanggal Pengembalian = ");
		String stglAkhir = input.nextLine();

		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

		java.util.Date d1 = null;
		java.util.Date d2 = null;

		try {
			d1 = format.parse(stglAwal);
			d2 = format.parse(stglAkhir);

			double diff = d2.getTime() - d1.getTime();
			double diffDays = diff / (24 * 60 * 60 * 1000);
			if (diffDays > lamaPinjam) {
				int rubahdouble = (int) (diffDays);
				System.out.println((rubahdouble - lamaPinjam) * 500);
			} else {
				System.out.println("tepat waktu");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void Resolve4() {
		Scanner input = new Scanner(System.in);
		System.out.println("Masukan Kata = ");
		String kataAwal = input.next();

		int panjang = kataAwal.length();
		char[] tempArray = kataAwal.toCharArray();

		String kataBalik = "";

		for (int i = panjang - 1; i >= 0; i--) {
			kataBalik = kataBalik + tempArray[i];
		}
		if (kataAwal.equals(kataBalik)) {
			System.out.println("Yes!");
		} else {
			System.out.println("No!");
		}
		System.out.println(kataBalik);
	}
}
