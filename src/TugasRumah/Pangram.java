package TugasRumah;

import java.util.Scanner;

public class Pangram {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner (System.in);
		String kata = input.nextLine().replaceAll(" ", "").toLowerCase();
		
		String hasil = "";
		
		for (char i = 'a'; i <= 'z'; i++) {
			if (kata.indexOf(i) != -1) {
				hasil += 1;
			}
		}
		if (hasil.length() == 26) {
			System.out.println("Pangram");
		} else {
			System.out.println("Not Pangram");
		}
		
	}

}
