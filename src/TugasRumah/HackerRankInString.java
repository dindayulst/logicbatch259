package TugasRumah;

import java.util.Scanner;

public class HackerRankInString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner (System.in);
		
		int n = input.nextInt();
		while (n-- > 0) {
			String s1 = input.next();
			String s2 = "hackerrank";
			int a = s1.length();
			int b = s2.length();
			
			char p[] = s1.toCharArray();
			char q[] = s2.toCharArray();
			
			int [][] tempArray = new int [b + 1] [a + 1];
			
			for (int i = 1; i <= b; i++) {
				for (int j = 1; j <= a; j++) {
					if (q[i - 1] == p[j - 1]) {
						tempArray [i][j] = tempArray[i-1][j-1] + 1;
					} else {
						tempArray[i][j] = Math.max(tempArray[i-1][j], tempArray[i][j-1]);
					}
				}
			}
			int result = tempArray[b][a];
			if (result == b) {
				System.out.println("YES");
			} else {
				System.out.println("NO");
			}
		}

	}

}
