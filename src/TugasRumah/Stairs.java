package TugasRumah;

import java.awt.print.Printable;
import java.util.Scanner;

public class Stairs {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int n = input.nextInt();
		
		int tempArray [] = new int [n];
		
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <=n-1; j++) {
				System.out.print(" ");
			}
			for (int k = 1; k <= i; k++) {
				System.out.print("#");
			}
			System.out.println();
		}
	}
}
