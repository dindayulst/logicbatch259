package TugasRumah;

import java.util.Scanner;

public class MakingAnagram {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner (System.in);
		
		String pertama = input.next();
		String kedua = input.next();
		
		char [] arrayPertama = new char [pertama.length()];
		char [] arrayKedua = new char [kedua.length()];
		
		for (int i = 0; i < pertama.length(); i++) {
			arrayPertama[i] = pertama.charAt(i);
		}
		
		for (int i = 0; i < kedua.length(); i++) {
			arrayKedua[i] = kedua.charAt(i);
		}
		
		int hitung = 0;
		for (int i = 0; i < pertama.length(); i++) {
			for (int j = 0; j < kedua.length(); j++) {
				if (arrayPertama[i] == arrayKedua[j]) {
					hitung++;
					arrayKedua [j] = '0';
					break;
				}
			}
		}
		System.out.println((pertama.length()+kedua.length()) - (2*hitung));
	}

}
