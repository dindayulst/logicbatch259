package TugasRumah;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class TimeConversion {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Masukan jam = ");
		String x = input.nextLine().toLowerCase();

		DateFormat formatAwal = new SimpleDateFormat("hh:mm:ssaa");
		DateFormat formatAkhir = new SimpleDateFormat("HH:mm:ss");
		
		Date date = null;
		String output = null;
		
		try {
		date = formatAwal.parse(x);
		output = formatAkhir.format(date);
		System.out.println(output);
		} catch (Exception z) {
			z.printStackTrace();
		}
	}

}
