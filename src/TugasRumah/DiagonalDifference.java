package TugasRumah;

import java.util.Arrays;
import java.util.Scanner;

public class DiagonalDifference {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner (System.in);
		System.out.print("Masukan jumlah Array 2 Dimensi = ");
		int n = input.nextInt();
		int tempArray [][] = new int [n][3];
		int x = 1;
		
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < 3 ; j++) {
				tempArray[i][j] = x;
				System.out.print(x + " ");
				x +=1;
			}
			System.out.println();
		}
		
		int diagonal1 = 0;
		diagonal1 = tempArray[0][0] + tempArray[1][1] + tempArray[2][2];
		
		int diagonal2 = 0;
		diagonal2 = tempArray[0][2] + tempArray[1][1] + tempArray[2][0];
		
		System.out.println();
		System.out.println((diagonal1 + diagonal2));
	}

}
