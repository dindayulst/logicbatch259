package TugasRumah;

import java.util.Scanner;

public class Soal6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Input = ");

		String kata = input.nextLine();
		String[] tempSplit = kata.split(" ");// menghilangkan spasi pada kata yang diinput
		String x = "";

		for (int i = 0; i < tempSplit.length; i++) {
			char[] tempArray = tempSplit[i].toCharArray();
			for (int j = 0; j < tempArray.length; j++) {
				if (j % 2 == 1) {
					x += "*";
				} else {
					x += tempArray[j];
				}
			}
			x += " ";
		}
		System.out.println(x);
	}
}
