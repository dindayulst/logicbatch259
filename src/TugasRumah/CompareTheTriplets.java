package TugasRumah;

import java.util.Scanner;

public class CompareTheTriplets {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner (System.in);
		int sizeArray = 3;
		int arrayA [] = new int [sizeArray];
		int arrayB [] = new int [sizeArray];
		
		for (int i = 0; i < sizeArray; i++) {
			int nilaiA = input.nextInt();
			arrayA[i] = nilaiA;
		}
		
		for (int i = 0; i < sizeArray; i++) {
			int nilaiB = input.nextInt();
			arrayB[i] = nilaiB;
		}
		
		int scoreA = 0;
		int scoreB = 0;
		
		for (int i = 0; i < sizeArray; i++) {
			if (arrayA[i] > arrayB[i]) {
				scoreA++;
			} else if (arrayB[i] > arrayA[i]) {
				scoreB++;
			}
		}
		System.out.println(scoreA + " " + scoreB);
	}

}
