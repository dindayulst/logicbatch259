package TugasRumah;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Jam {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Masukan jam = ");
		String x = input.nextLine().toLowerCase();

		DateFormat formatAwal = new SimpleDateFormat("hh:mm:ssaa");
		DateFormat formatAkhir = new SimpleDateFormat("HH:mm:ss");
		
		Date date = null;
		String output = null;
		
		try {
		date = formatAwal.parse(x);
		output = formatAkhir.format(date);
		System.out.println(output);
		} catch (Exception z) {
			z.printStackTrace();
		}

//		System.out.print("Masukan format 12 jam  = ");
//		String waktu = input.next().trim();
//		//substring digunakan untuk mengcut suatu string
//		String jam = waktu.substring(0, 2);
//		String menit = waktu.substring(3, 5);
//		String detik = waktu.substring(6, 8);
//		String kode = waktu.substring(8);
//		
//		//mengubah tipe data string (jam) menjadi integer
//		int jamInteger = Integer.parseInt(jam);
//		
//		if (kode.equals("PM") || kode.equals("pm")) {
//			if (jamInteger > 1 && jamInteger < 12) {
//			jamInteger = jamInteger+12;
//			System.out.println(jamInteger + ":" +menit+ ":" + detik + kode);
//			} else if (jamInteger > 12) {
//				System.out.println("Format jam yang anda masukan salah");
//			}
//		} else {
//			System.out.print(jam + ":" +menit+ ":" +detik+ kode);
//		}	
	}

}
