package TugasRumah;

import java.util.Arrays;
import java.util.Scanner;

public class PlusMinus {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner (System.in);
		System.out.print("Masukan panjang Array = ");
		int panjangArray = input.nextInt();
		int tempArray[] = new int [panjangArray];
		
		float positif = 0;
		float negatif = 0;
		float zero = 0;
		
		for (int i = 0; i < panjangArray; i++) {
			int isi = input.nextInt();
			tempArray[i] = isi;
			if (isi > 0) {
				positif++;
			} if (isi < 0) {
				negatif++;
			} if (isi == 0) {
				zero++;
			}
		}
		System.out.println((positif/panjangArray));
		System.out.println((negatif/panjangArray));
		System.out.println((zero/panjangArray));
	}
}
