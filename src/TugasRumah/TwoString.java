package TugasRumah;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class TwoString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner (System.in);
		Set<Character> s1Set = new HashSet<>();
		Set<Character> s2Set = new HashSet<>();
		System.out.println("Masukan Kata Pertama = ");
		String s1 = input.nextLine();
		for (char c : s1.toCharArray()) {
			s1Set.add(c);
		}
		System.out.println("Masukan Kata Kedua = ");
		String s2 = input.nextLine();
		for(char c : s2.toCharArray()) {
			s2Set.add(c);
		}
		s1Set.retainAll(s2Set);
		
		if (!s1Set.isEmpty()) {
			System.out.println("YES!");
		}else {
			System.out.println("NO!");
		}
	}
}
