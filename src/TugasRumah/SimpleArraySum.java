package TugasRumah;

import java.util.Arrays;
import java.util.Scanner;

public class SimpleArraySum {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Masukan Jumlah Array = ");
		int n = input.nextInt();
		int tempArray [] = new int [n];
		
		int total = 0;
		for (int i = 0; i <n; i++) {
			int isi = input.nextInt();
			tempArray[i] = isi;
			total +=isi;
		}
		System.out.println(total);
	}
}
