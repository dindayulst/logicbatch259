package TugasRumah;

import java.util.Scanner;

public class MinMaxSum {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner (System.in);
		System.out.print("masukan n = ");
		int n = input.nextInt();
		
		int tempArray[] = new int [n];
		
		for (int i = 0; i < n; i++) {
 			int banyak = input.nextInt();
 			tempArray[i] = banyak;
		}
		
		int min = tempArray[0];
		int sum = 0;
		int max = 0;
		
		for (int i = 0; i < tempArray.length; i++) {
			sum += tempArray[i];
			if (tempArray[i] < min) {
				min = tempArray[i];
			}
			if (tempArray[i] > max) {
				max = tempArray[i];
			}
		}
		System.out.println((sum - min) + " " + (sum - max));

	}
}

