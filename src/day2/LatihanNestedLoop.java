package day2;

import java.util.Scanner;

public class LatihanNestedLoop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner (System.in);
		System.out.print("Masukan nilai n = ");
		
		int n = input.nextInt();
		System.out.print("Masukan nilai n2 = ");
		int n2 = input.nextInt(); 
				
		int [] [] tempArray = new int [2] [n];
		int x = 1;
		
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
				tempArray[i][j] = j;
				System.out.print(tempArray[i][j] + " ");
				} else {
					tempArray[i][j] = x;
					x *= n2;
					System.out.print(tempArray[i][j] + " ");
				}
				
			}
			System.out.println();
		}
		input.close();
	}

}
