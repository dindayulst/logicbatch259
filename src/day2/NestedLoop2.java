package day2;

import java.util.Scanner;

public class NestedLoop2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner (System.in);
		System.out.print("Masukan nilai n = ");
		
		int n = input.nextInt();
		System.out.print("Masukan nilai n2 = ");
		int n2 = input.nextInt();
		
		int [][] tempArray = new int [2][n];
		int x = 1;
		
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else if (j % 3 == 2)  {
					tempArray [i][j] = (x * -1);
					System.out.print(tempArray[i][j] + " ");
					x *= n2;
				} else {
					tempArray[i][j] = x;
					System.out.print(tempArray[i][j] + " ");
					x *= n2;
				}
				
			} System.out.println();
			
		}
		

	}

}
