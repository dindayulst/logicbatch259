package day2;


import java.util.Arrays;
import java.util.Scanner;

public class LatihanLooping {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner (System.in);
		System.out.println("Masukan Nilai n :");
		
		int n = input.nextInt();
		
		int x = 1;
		int [] tempArray = new int[n]; //array integer
//		String [] tempArrayString = new String [5]; //aray String
		
		//untuk memasukan data ke dalam array
		for (int i = 0; i < tempArray.length; i++) {
			tempArray[i]= x;
			x++;
			//untuk mencetak array tanpa menggunakan for
			System.out.print(tempArray[i] + "");
		}
		
		//untuk mencetak array menggunakan for
//		for (int i = 0; i < tempArray.length; i++) {
//			System.out.println(tempArray[i] + "");
//		}
		
		//untuk mencetak array menggunakan cara lain
		System.out.print(Arrays.toString(tempArray));

		//perulangan for biasa
//		for (int i = 0; i < n; i++) {
//			x++;
//			System.out.print(x + "");
//			x+=1;
//			if (i  % 2 == 0) {
//				System.out.println(i+ " "); // mencetak bilangan genap
//			}
//		}
	}

}
