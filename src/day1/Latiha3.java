package day1;

import java.util.Scanner;

public class Latiha3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.println("Belanja :");
		int belanja = input.nextInt();
		
		System.out.println("Ongkos Kirim :");
		int ongkir = input.nextInt();
		
		int disOngkir = 0;
		int disBel = 0;
		
		if (belanja >= 100000) {
			disOngkir = 10000;
			disBel = 20000;
			
		} else if (belanja >= 50000 ){
			disOngkir = 10000;
			disBel = 10000;
			
		} else if (belanja >=30000) {
			disOngkir = 5000;
			disBel = 5000;
		} 
		
		System.out.println("Belanja : " + belanja);
		System.out.println("Ongkos Kirim :" + ongkir);
		System.out.println("Diskon Ongkir : " + disOngkir);
		System.out.println("Diskon Belanja : " + disBel);
		System.out.println("Total Belanja : " + (belanja + ongkir - disOngkir - disBel));
		
	}

}
