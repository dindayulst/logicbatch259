package day1;

import java.util.Scanner;

public class Latihan2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner (System.in);
		
		System.out.println("Belanja :");
		int belanja = input.nextInt();
		
		System.out.println("Jarak :");
		int jarak = input.nextInt();
		
		System.out.println("Masukan Kode Promo :");
		String kode = input.next();
		
		int diskon = 0;
		int ongkir = 0;
		
		if (kode.equals("JKTOVO")) {
			if (belanja >= 30000) {
				diskon = belanja * 40 /100;
			} else if (belanja < 30000) {
				diskon = 0;
			} 
		} if (diskon > 30000) {
			diskon = 30000;
		}
		if ( jarak > 5) {
			ongkir = jarak * 1000;
		} else if (jarak <= 5){
			ongkir = 5000;
		}
		
		System.out.println("Belanja : " +belanja);
		System.out.println("Diskon :" + diskon);
		System.out.println("Ongkir :" + ongkir);
		System.out.println("Total Belanja : " + (belanja + ongkir - diskon));
		
		
	}

}
