package day1;

import java.util.Scanner;

public class LatihanSoal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Pulsa : ");
		
		int pulsa = input.nextInt();
		String point = "";
		
		if (pulsa >= 100000) {
			point = "800";
		} else if (pulsa >= 50000) {
			point = "400";
		} else if (pulsa >= 25000) {
			point = "200";
		} else if (pulsa >= 10000) {
			point = "80";
		} else {
			point = "0";
		}
		System.out.println("point : " + point);
	}

}
