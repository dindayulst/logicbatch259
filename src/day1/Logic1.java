package day1;

import java.util.Scanner;

public class Logic1 {
	int id = 1;
	String nama = "Dinda";
	int a = 10;
	int b = 20;
	int arr[] = {10, 20, 30};
	boolean salah = false; //nilainya cuma true or false
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Logic1 logic = new Logic1();
		System.out.println("Nama = " + logic.nama);
		System.out.println("Jumlah = " + (logic.a + logic.b));
		System.out.println(logic.salah);
		
		double seribu = 1000.0;
		double duaribu = 2000.0;
		
		System.out.println(seribu + duaribu); //memanggil variable di main
		
		String strA = "5";
		String strB = "3";
		
		System.out.println(strA + strB); //tipe data string tidak bisa ditambah
		System.out.println(Integer.parseInt(strA) + Integer.parseInt(strB)); //Integer.parseInt digunakan untuk mengubah string menjadi integer
		
		Scanner input = new Scanner(System.in); //untuk inputan
		System.out.println("Masukan Nilai 1: ");
		int numberOne = input.nextInt();
		System.out.println("Nilai 1 = " + numberOne);//cara mencetak sesuatu yang diinput yaitu "numberOne" 
		
		System.out.println("Masukan Nilai 2 : ");
		int numberTwo = input.nextInt();
		System.out.println("Nilai 2 = "+ numberTwo);
		
		int jumlah = numberOne + numberTwo;
		
		//System.out.println("Jumlah Nilai Tambah = " + (numberOne + numberTwo) );
		System.out.println("Jumlah = "+ jumlah);
		
	}

}
