package SimulasiFT1;

import java.util.Scanner;

import SimulasiFT1.*;

public class Main {

static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args) {
		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println(("Enter the number of case"));
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					Case01.Resolve1();
					break;
				case 2:
					Case01.Resolve2();
					break;
				case 3:
					Case01.Resolve3();
					break;
				case 4 :
					Case01.Resolve4();
					break;
				case 5 :
					Case01.Resolve5();
					break;
				case 6 :
					Case01.Resolve6();
				case 7 :
					Case01.Resolve7();
					break;
				case 8 :
					Case01.Resolve8();
					break;
				case 9:
					Case01.Resolve9();
				default:
					System.out.println("Case is not available");
					break;
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.println("Continue ?");
			answer = input.nextLine();

		}
	}

}
