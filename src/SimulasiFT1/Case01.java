package SimulasiFT1;

import java.util.Arrays;
import java.util.Scanner;

public class Case01 {
	
	public static void Resolve1() {
		Scanner input = new Scanner (System.in);
		System.out.println("Masukan Nilai n = ");
		int n = input.nextInt();
		
		for (int i = 1; i <= n; i++) {
			if(i % 2 == 1) {
				System.out.print(i + " ");
			}
		}
		System.out.println();
		
		for (int j = 1; j <= n; j++) {
			if(j % 2== 0) {
				System.out.print(j + " ");
			}
		}
	}
	
	public static void Resolve2() {
		Scanner input = new Scanner (System.in);
		System.out.println("Masukan kata = ");
		String kata = input.nextLine().toLowerCase().replaceAll(" ", "");
		
		String [] temp = kata.split(" ");
		char [] array = kata.toCharArray();
		String vokal = "";
		String konsonan = "";
		
		Arrays.sort(array);
		for (int i = 0; i < array.length; i++) {
			if (array[i] == 'a' || array[i] == 'i' || array[i] == 'u' || array[i] == 'e' || array[i] == 'o') {
				vokal += array[i];
			} else {
				konsonan += array[i];
			}
		}
		System.out.println("huruf vokal = " + vokal);
		System.out.println("huruf konsonan = " + konsonan);
}
	
	public static void Resolve3() {
		Scanner input = new Scanner (System.in);
		System.out.println("input n = ");
		int n = input.nextInt();
		int x = 100;
		
		for (int i = 0; i < n; i++) {
			System.out.println(x + " adalah 'Si Angka 1' ");
			x = x+3;
		}
		
	}
	
	public static void Resolve4() {
		Scanner input = new Scanner(System.in);
		System.out.println("Masukan Jarak = ");
		
		int tempJarak[] = new int[4];
		double total = 0;
		double bensin = 0;
		
		for (int i = 0; i <4; i++) {
			int jarak = input.nextInt();
			tempJarak[i] = jarak;
		}
		input.nextLine();
		
		System.out.println("Masukan Rute = ");
		String inputIndexSplit[] = input.nextLine().split(" ");
		
		for (int i = 0; i < inputIndexSplit.length; i++) {
			total += tempJarak[Integer.parseInt(inputIndexSplit[i])-1];
			}
		System.out.println("Jarak = " + total / 1000 + "Km");
		bensin = total / 2500;
		System.out.println(Math.ceil(bensin) + "Km");
	
	}
	
	public static void Resolve5() {
		Scanner input = new Scanner (System.in);
		System.out.println("Laki - Laki Dewasa = ");
		int lakiDewasa = input.nextInt();
	
		System.out.println("Perempuan Dewasa = ");
		int peremDewasa = input.nextInt();
		
		System.out.println("Remaja = ");
		int remaja = input.nextInt();
		
		System.out.println("Anak - anak = ");
		int anak = input.nextInt();
		
		System.out.println("Balita = ");
		int balita = input.nextInt();
		
		int porsiLaki = lakiDewasa*2;
		
		int total = lakiDewasa + peremDewasa + remaja + anak + balita;
		
		int porsi = (lakiDewasa*2) + peremDewasa + remaja + anak + balita;
		
		System.out.println(porsi + " porsi");
		
		if (total > 5 && total % 2 == 1) {
			 porsi = (lakiDewasa*2) + (peremDewasa*2) + remaja + anak + balita;
		}
		System.out.println(porsi + " porsi");
	}
	
	public static void Resolve6() {
		Scanner input = new Scanner (System.in);
		System.out.println("Masukan Pin : ");
		String pin = input.next();
		
		System.out.println("Uang Yang Di Setor : ");
		int uangSetoran = input.nextInt();
		input.nextLine();
		
		System.out.println("Pilihan Transfer : ");
		String pilihanTransfer = input.next();
		
		if (pilihanTransfer.equals("1")) {
			System.out.println("Masukan Rekening Tujuan : ");
			String rekTujuan = input.next();
			
			System.out.println("Masukan Nominal Transfer : ");
			int nominalTransfer = input.nextInt();
			input.nextLine();
			
			int saldo = 0;
			saldo = uangSetoran - nominalTransfer;
			if (saldo > 0) {
				System.out.println("Transaksi Berhasil, Saldo Anda Saat ini = Rp" + saldo);
			} else {
				System.out.println("Saldo Anda Tidak Cukup");
			}
			
		}
		if (pilihanTransfer.equals("2")) {
			System.out.println("Kode Bank : ");
			String kodeBank = input.next();
			
			System.out.println("Masukan Rekekning Tujuan : ");
			String rekTujuan = input.next();
			
			System.out.println("Masukan Nominal Transfer : ");
			int nominalTransfer = input.nextInt();
			input.nextLine();
			
			int saldo = 0;
			saldo = uangSetoran - nominalTransfer - 7500;
			if (saldo > 0) {
				System.out.println("Transaksi Berhasil, Saldo Anda Saat ini = Rp" + saldo);
			} else {
				System.out.println("Saldo Anda Tidak Cukup");
			}
		}
		
	}
	
	public static void Resolve7() {
		
	}
	
	public static void Resolve8() {
		Scanner input = new Scanner (System.in);
		System.out.println("Masukan panjang deret = ");
		int n = input.nextInt();
		int [][] tempArray = new int [2][n];
		int x =0;
		int y=1;
		int sum = 0;
		
		for (int i = 0; i <2; i++) {
			for (int j = 0; j <n; j++) {
				if (i == 0) {
					tempArray[i][j]= x;
					System.out.print(tempArray[i][j] + " ");
					x = x+2;
				} else {
					tempArray[i][j]=y;
					System.out.print(tempArray[i][j]+ " ");
					y= y + 2;
				}
			} System.out.println();
		}
	}
	
	public static void Resolve9() {
		Scanner input = new Scanner (System.in);
		String urutan = input.next().toUpperCase();
		
		int lembah = 0;
		int gunung = 0;
		
		for (int i = 0; i < urutan.length(); i+=2) {
			String sub1 = urutan.substring(i, i+2);
			if(sub1.equals("NT")) {
				gunung += 1;
			}if(sub1.equals("TN")) {
				lembah +=1;
			}
		}
		System.out.println("Gunung = " + gunung);
		System.out.println("Lembah = " + lembah);
	}
}
